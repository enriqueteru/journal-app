import { initializeApp } from "firebase/app";
import { GoogleAuthProvider } from "firebase/auth";
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { getFirestore } from "firebase/firestore";


const firebaseConfig = {
  apiKey: "AIzaSyBBF81kqrGQvzuIgXfaHKC1FMfvWaEgVqw",
  authDomain: "journal-app-da1c4.firebaseapp.com",
  projectId: "journal-app-da1c4",
  storageBucket: "journal-app-da1c4.appspot.com",
  messagingSenderId: "855204493741",
  appId: "1:855204493741:web:c0935dec5a3bc433018924",
  measurementId: "G-DRGGMTBZ1C"
};

 
const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore();
const googleProvider = new GoogleAuthProvider(); 
 
export{  
    firebaseApp,  
    googleProvider,
    db,
};