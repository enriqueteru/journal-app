import { types } from "../types/types";
import { firebaseApp, googleProvider } from "../../firebase/firebase-config";
import {
  createUserWithEmailAndPassword,
  getAuth,
  signInWithEmailAndPassword,
  signInWithPopup,
  updateProfile,
} from "firebase/auth";
import "firebase/compat/auth";
import "firebase/compat/firestore";
import Swal from "sweetalert2";
import { startLoading, endLoading } from "./uiActions";

export const startLoginEmailPassword = (email, password) => {
  return (dispatch) => {
    dispatch(startLoading());
    const auth = getAuth(firebaseApp);
    signInWithEmailAndPassword(auth, email, password)
      .then(({ user }) => {
        dispatch(login(user.uid, user.displayName));
        dispatch(endLoading());
      })
      .catch((e) => {
        dispatch(endLoading());
        Swal.fire("Error", e.code, "error");
      });
  };
};

export const startRegisterWithEmailPassword = (email, password, name) => {
  return (dispatch) => {
    const auth = getAuth(firebaseApp);
    createUserWithEmailAndPassword(auth, email, password)
      .then(async ({ user }) => {
        await updateProfile(user, { displayName: name });
        dispatch(login(user.uid, user.displayName));

      })
      .catch((e) => {
        Swal.fire("Error", e.code, "error");
      });
  };
};

export const startGoogleLogin = () => {
  return (dispatch) => {
    const auth = getAuth(firebaseApp);
    signInWithPopup(auth, googleProvider)
    .then(({ user }) => {
      dispatch(login(user.uid, user.displayName));
    })
    .catch((e)=> Swal.fire("Error", e.code, "error"))
  };
};

export const login = (uid, displayName) => {
  return {
    type: types.login,
    payload: {
      uid,
      displayName,
    },
  };
};

export const startLogout = () => {
  return async (dispatch) => {
    const auth = getAuth(firebaseApp);
    await auth.signOut();
    dispatch(logout());
  };
};

export const logout = () => ({
  type: types.logout,
});
