import { types } from "../types/types";

export const setError = (msgError) => {
  return {
    type: types.uiSetError,
    payload: {
      msgError: msgError,
    },
  };
};

export const removeError = () => {
  return {
    type: types.uiRemoveError,
  };
};

export const startLoading = () => {
  return {
    type: types.uiStartLoading,
  };
};

export const endLoading = () => {
  return {
    type: types.uiEndLoading,
  };
};

export const openMenu = () => {
  return {
    type: types.uiMenuSideBarActive,
  };
};

export const closeMenu = () => {
  return {
    type: types.uiMenuSideBarClose,
  };
};
