import { db } from "../../firebase/firebase-config";
import { collection, addDoc, updateDoc, doc, deleteDoc } from "firebase/firestore";
import Swal from "sweetalert2";
import { types } from "../types/types";
import { loadNotes } from "../../helpers/loadNotes";
import { fileUpload } from "../../helpers/fileUpload";

export const startNewNote = () => {
  return async (dispatch, getState) => {
    const {
      auth: { uid },
    } = getState();
    const newNote = {
      title: "",
      body: "",
      date: new Date().getTime(),
    };
    try {
      const doc = await addDoc(collection(db, `${uid}/journal/notes`), newNote);
      dispatch(activeNote(doc.id, newNote));
      dispatch(addNewNote(doc.id, newNote));
    } catch (err) {
     console.log(err)
    }
  };
};

export const activeNote = (id, note) => ({
  type: types.NoteActive,
  payload: {
    id,
    ...note,
  },
});

export const addNewNote = (id, note)=> ({ 
  type: types.noteAddNew, 
  payload:{ 
    id,
    ...note,
  }
})

export const startLoadingNotes = (uid) => {
  return async (dispatch) => {
    const notes = await loadNotes(uid);
    dispatch(setNotes(notes));
  };
};

export const setNotes = (notes) => ({
  type: types.NotesLoad,
  payload: notes,
});

export const startSaveNote = (note) => {
  return async (dispatch, getState) => {
    try {
      const {
        auth: { uid },
      } = getState();

      if (!note.url) {
        delete note.url;
      }

      const noteToFirestore = { ...note };
      delete noteToFirestore.id;
      const noteRef = doc(db, `${uid}/journal/notes/${note.id}`);
      await updateDoc(noteRef, noteToFirestore);
      dispatch(reloadNotes(note.id, note));
      Swal.fire("saved", note.title, "success");
    } catch (e) {
      Swal.fire("error", "Something go wrong...", "error");
    }
  };
};

export const reloadNotes = (id, note) => ({
  type: types.NotesUpdate,
  payload: {
    id,
    note: {
      id,
      ...note,
    },
  },
});

export const startNoteUploadFile = (file) => {
  return async (dispatch, getState) => {
    const { active } = getState().notes;

    Swal.fire({
      title: "Uploading file",
      text: "Please wait...",
      allowOutsideClick: false,
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      },
    });

    const fileUrl = await fileUpload(file);
    active.url = fileUrl;
    dispatch(startSaveNote(active));
  };
};

export const startDeleteNote = (id) => {
  return async (dispatch, getState) => {
    const uid = getState().auth.uid;
    const noteRef = doc(db, `${uid}/journal/notes/${id}`);
    await deleteDoc(noteRef);
    dispatch(deleteNote(id));
  };
}


export const deleteNote = (id) => ({ 
type: types.noteDelete,
payload: id,
})

export const notesLogout = ()=> ({ 
   type: types.noteLogoutClenning,
   payload: [],
})