import { createStore, combineReducers, compose, applyMiddleware} from "redux";
import thunk from 'redux-thunk'
import { authReducer } from '../reducers/authReducer'
import { composeWithDevTools } from 'redux-devtools-extension';
import { notesReducer } from "../reducers/notesReducer";
import { uiReducer } from "../reducers/uiReducer";


const reducers = combineReducers({
  auth: authReducer,
  ui: uiReducer,
  notes: notesReducer,
});
export const store = createStore(reducers, compose(applyMiddleware(thunk) ,composeWithDevTools()) );

