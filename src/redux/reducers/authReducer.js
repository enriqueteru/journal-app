import { types } from "../types/types";

export const authReducer = (state = {}, action) => {
  switch (action.type) {
    case types.login:
      return {
        uid: action.payload.uid,
        name: action.payload.displayName,
      };

    case types.logout:
      return {};

      case types.register:
        return{ 
          uid: action.payload.uid,
          name: action.payload.name,
          email: action.payload.email,
          password: action.payload.password,

        }
        

    default:
      return state;
  }
};
