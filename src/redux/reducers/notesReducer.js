import { types } from "../types/types";

const INITIAL_STATE = {
  notes: [],
  active: null,
};
export const notesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.NoteActive:
      return {
        ...state,
        active: {
          ...action.payload,
        },
      };
    case types.NotesLoad:
      return {
        ...state,
        notes: [...action.payload],
      };

    case types.NotesUpdate:
      return {
        ...state,
        notes: state.notes.map((note) =>
          note.id === action.payload.id ? action.payload.note : note
        ),
      };

    case types.noteDelete:
      return {
        ...state,
       active: null,
       notes: state.notes.filter(note => note.id !== action.payload)
      };

    case types.noteLogoutClenning:
      return {
        ...state,
       active: null,
       notes: state.notes =action.payload,
      };

      case types.noteAddNew:
        return {
          ...state,
          notes: [ action.payload, ...state.notes]
          
        };

    default:
      return state;
  }
};
