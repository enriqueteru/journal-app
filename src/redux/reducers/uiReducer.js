import { types } from "../types/types";

const INITIAL_STATE = {
  loading: false,
  msgError: null,
  menuActive: true,
};

export const uiReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.uiSetError:
      return {
        ...state,
        msgError: action.payload,
      };

    case types.uiRemoveError:
      return {
        ...state,
        msgError: null,
      };

    case types.uiStartLoading:
      return {
        ...state,
        loading: true,
      };
    case types.uiEndLoading:
      return {
        ...state,
        loading: false,
      };
    case types.uiMenuSideBarActive:
      return {
        ...state,
        menuActive: true,
      };

    case types.uiMenuSideBarClose:
      return {
        ...state,
        menuActive: false,
      };

    default:
      return state;
  }
};
