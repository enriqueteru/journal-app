export const types = {
  login: "[Auth] login",
  logout: "[Auth] logout",
  register: "[Auth] register",

  uiSetError: "[UI] set Error",
  uiRemoveError: "[UI] Remove Error",
  uiStartLoading: "[UI] Start Loading",
  uiEndLoading: "[UI] End Loading",
  uiMenuSideBarActive : "[UI] ActiveSideBar",
  uiMenuSideBarClose : "[UI] CloseSideBar",

  noteAddNew: "[Notes] add new",
  NoteActive: "[Notes active",
  NotesLoad: "[Notes] load",
  NotesUpdate: "[Notes] updated",
  NotesFileUrl: "[Notes] update img url",
  noteDelete: "[Notes] delete",
  noteLogoutClenning: "[Notes] logout cleaning",
};
