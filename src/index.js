import React from 'react';
import ReactDOM from 'react-dom';
import JournalApp from './JournalApp'
import './styles/styles.scss'
import { BrowserRouter as Router} from 'react-router-dom';


ReactDOM.render(
<Router>
    <JournalApp />
</ Router >,

  document.getElementById('root')
);
