import React from 'react';
import AppRouter from './routes/AppRouter';
import { Provider } from 'react-redux';
import { store } from './redux/store/store';
const JournalApp = () => {
  return <div>
    <Provider store={store}>
      <AppRouter />
    </Provider>
  </div>
}

export default JournalApp;
