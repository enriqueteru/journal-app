import React from "react";
import Sidebar from "./Sidebar";
import NothingSeleted from "./NothingSeleted";
import NoteScreen from "../notes/NoteScreen";
import { useSelector } from "react-redux";
const JournalScreen = () => {
const { active } = useSelector((state) => state.notes);
const {menuActive } = useSelector((state) => state.ui);

  return (

      <div className="journal">
 
      <Sidebar menuActive={menuActive} />
    
      <main className="journal__main" >
        {!active && <NothingSeleted menuActive={menuActive}  />}
        {active && <NoteScreen />}
      </main>
    </div>
  );
};

export default JournalScreen;
