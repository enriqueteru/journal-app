import React from "react";
import { useDispatch } from "react-redux";
import { startNewNote } from "../../redux/actions/notesActions";
import { openMenu } from "../../redux/actions/uiActions";
const NothingSeleted = ({ menuActive }) => {
  const dispatch = useDispatch();

  const handlesOpenMenu = () => {
    dispatch(openMenu());
  };

  const handleAddNew = () => {
    dispatch(startNewNote());
  };

  return (
    <div className="nothing ">
      
      <p className="nothing__p">Select Something or create a new entry</p>
     
      <button onClick={handleAddNew}  className="btn-primary">New Entry</button>
    
      
      {!menuActive && (
        <button className="btn-save" onClick={handlesOpenMenu}>
         All Entries{" "}
        </button>
      )}
    </div>
  );
};

export default NothingSeleted;
