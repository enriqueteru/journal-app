import React from "react";
import { FaBars, FaPowerOff } from "react-icons/fa";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { startLogout } from "../../redux/actions/authActions";
import { notesLogout, startNewNote } from "../../redux/actions/notesActions";
import { closeMenu } from "../../redux/actions/uiActions";
import JournalEntries from "./JournalEntries";

const Sidebar = ({ menuActive }) => {

  const dispatch = useDispatch();
  const { name } = useSelector((state) => state.auth);

  const handleLogout = () => {
    dispatch(notesLogout());
    dispatch(startLogout());
  };

  const handleAddNew = () => {
    dispatch(startNewNote());
  };

  const handlesCloseMenu = () => {
    dispatch(closeMenu());
  };

  return (
    <>
      {menuActive && (
        <aside className="journal__sidebar">
          <div className="journal__menu-user">
            <p>{name}</p>
            <div>
            <FaPowerOff className="big-ico" onClick={handleLogout} />
            {menuActive && (
              <>
                <FaBars  className="big-ico" onClick={handlesCloseMenu} />
              </>
            )}
          </div>
          </div>

          <div onClick={handleAddNew} className="journal__new">

            <p>New entry</p>
          </div>

          <JournalEntries />
        </aside>
      )}
      
    </>
  );
};

export default Sidebar;
