import React from "react";
import moment from "moment";
import { useDispatch } from "react-redux";
import { activeNote } from "../../redux/actions/notesActions";

const JournalEntry = ({ id, date, title, body, url}) => {
  const noteDate = moment(date);
  const dispatch = useDispatch();

  const handleActiveNote = () => {
    dispatch(activeNote(id, { date, title, body, url }));
  };

  return (
    <div onClick={handleActiveNote} className="journal__entry">
      {url && (
        <div
          className="journal__entry-img"
          style={{
            backgroundImage: `url(${url})`,
          }}
          alt=""
        ></div>
      )}

      <div className="journal__entry-content">
        <h4>{title}</h4>
        <p> {body} </p>
      </div>

      <div className="journal__entry-date">
        <h4> {noteDate.format("Do")} </h4>{" "}
        <span> {noteDate.format("MMM")}</span>{" "}
        <span> {noteDate.format("YYYY")}</span>
      </div>
    </div>
  );
};

export default JournalEntry;
