import React, { useEffect, useRef } from "react";
import NotesAppBar from "./NotesAppBar";
import { useForm } from "../../hooks/useForms";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import {
  activeNote,
  startDeleteNote,
  startSaveNote,
} from "../../redux/actions/notesActions";
//Journal

const NoteScreen = () => {
  const dispatch = useDispatch();
  const { active: noteActive } = useSelector((state) => state.notes);
  const [formValues, handleInputChange, reset] = useForm(noteActive);
  const { body, title, id } = formValues;

  const activeId = useRef(noteActive.id);

  useEffect(() => {
    if (noteActive.id !== activeId.current) {
      reset(noteActive);
      activeId.current = noteActive.id;
    }
  }, [noteActive, reset]);

  useEffect(() => {
    dispatch(activeNote(formValues.id, { ...formValues }));
  }, [dispatch, formValues]);

  const handleSaveNote = (ev) => {
    ev.preventDefault();
    dispatch(startSaveNote(noteActive));
  };

  const handleDeleteNote = () => {
    dispatch(startDeleteNote(id));
  };

  return (
    <div className="notes">
      <NotesAppBar
        handleSaveNote={handleSaveNote}
        handleDeleteNote={handleDeleteNote}
      />

      <div className="notes__content">
        <form
          onSubmit={handleSaveNote}
          style={{ display: "flex", flexDirection: "column" }}
        >
          <input
            type="text"
            name="title"
            onChange={handleInputChange}
            value={title}
            placeholder="The awesome title goes here"
            className="notes__title-input"
          />

          <textarea
            name="body"
            onChange={handleInputChange}
            value={body}
            placeholder="What happens today?"
            cols="30"
            rows="10"
            className="notes__textarea"
          ></textarea>
        </form>
        {noteActive.url && (
          <div className="notes__images">
            <img
              className="notes__images-img"
              src={noteActive.url}
              alt="images"
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default NoteScreen;
