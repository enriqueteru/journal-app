import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { startNoteUploadFile } from "../../redux/actions/notesActions";
import moment from "moment";
import { FaBars } from "react-icons/fa";
import { openMenu } from "../../redux/actions/uiActions";

const NotesAppBar = ({ handleSaveNote, handleDeleteNote }) => {
  const dispatch = useDispatch();
  const { active } = useSelector((state) => state.notes);
  const { menuActive } = useSelector((state) => state.ui);
  const noteDate = moment(active.date);
  const handleimgCLick = () => {
    document.querySelector("#fileSelector").click();
  };

  const handleFileChange = (ev) => {
    const file = ev.target.files[0];
    if (file) {
      dispatch(startNoteUploadFile(file));
    }
  };


  const handlesOpenMenu = ()=>{ 
    dispatch( openMenu() )
  }

  return (
    <div className="appbar">
      <div style={{display:'flex'}}>
      {!menuActive &&
       <>
        <FaBars className="big-ico" onClick={handlesOpenMenu} />
       </>
        }
      <h4 className="date"> {noteDate.format("DD/MM/YYYY")}</h4>
      </div>
      <div style={{display:'flex'}} >
        <input
          type="file"
          name="file"
          id="fileSelector"
          style={{ display: "none" }}
          onChange={handleFileChange}
        />

        {active.url && (
          <button onClick={handleimgCLick} className="btn-primary">
         Picture
          </button>
        )}
        {!active.url && (
          <button onClick={handleimgCLick} className="btn-primary">
            Image
          </button>
        )}

        <button onClick={handleSaveNote} className="btn-primary">
          Save
        </button>
        <button onClick={handleDeleteNote} className="btn-danger">
          Delete
        </button>
      </div>
    </div>
  );
};

export default NotesAppBar;
