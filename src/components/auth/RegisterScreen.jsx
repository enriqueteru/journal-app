import React from "react";
import { useForm } from "../../hooks/useForms";
import { Link } from "react-router-dom";
import validator from "validator";
import { useDispatch, useSelector } from "react-redux";
import { removeError } from "../../redux/actions/uiActions";
import { startRegisterWithEmailPassword  } from "../../redux/actions/authActions";
import Swal from "sweetalert2";


const RegisterScreen = () => {
  const [values, handleInputChange] = useForm({
    name: "Enrique",
    email: "info@enrique.com",
    password: "123456789",
    password2: "123456789",
  });
  const { name, email, password, password2 } = values;

  const dispatch = useDispatch();
  const state = useSelector((state) => state.ui);
 const {msgError} = state


  const handleRegister = (ev) => {
    ev.preventDefault();

    if (!isFormValid()) {
      return false;
    }

    dispatch(removeError());
    dispatch(startRegisterWithEmailPassword(email, password, name))
  };

  const isFormValid = () => {
    if (validator.isEmpty(name)) {
      const msgError = "Invalid name";
      Swal.fire("Error", msgError, "error");
      return false;
    } else if (!validator.isEmail(email)) {
      const msgError = "Invalid email";
      Swal.fire("Error", msgError, "error");;
      return false;
    } else if (password !== password2 || password2 === "") {
      const msgError = "Invalid Password";
      Swal.fire("Error", msgError, "error");;
      return false;
    }

    return true;
  };

  return (
    <div>
      <div className="auth__box">
        <h2 className="auth__title ">Register</h2>
        {msgError && 
        
          <p> {msgError.msgError} </p>
        
        }
        <form onSubmit={handleRegister} className="auth__box">
          <input
            className="auth__box-input mb-5"
            type="text"
            name="name"
            onChange={handleInputChange}
            value={name}
            placeholder="name"
          />
          <input
            className="auth__box-input mb-5"
            type="text"
            name="email"
            onChange={handleInputChange}
            value={email}
            placeholder="email"
          />
          <input
            className="auth__box-input mb-5"
            type="password"
            name="password"
            onChange={handleInputChange}
            value={password}
            placeholder="password"
          />
          <input
            className="auth__box-input mb-5"
            type="password"
            name="password2"
            onChange={handleInputChange}
            value={password2}
            placeholder="repeat password"
          />
          <button className="btn-primary" type="submit">
            Register
          </button>
          <hr />
        </form>
        <Link to="../login">
          {" "}
          <p className="link"> Have account? Login </p>
        </Link>
      </div>
    </div>
  );
};

export default RegisterScreen;
