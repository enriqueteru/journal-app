import React from "react";
import { Link } from "react-router-dom";
import { useForm } from "../../hooks/useForms";
import { useDispatch, useSelector } from "react-redux";
import {
  startGoogleLogin,
  startLoginEmailPassword,
} from "../../redux/actions/authActions";

const LoginScreen = () => {
  const [formValues, handleInputChange] = useForm({
    email: "info@info.es",
    password: 123456,
  });

  const { email, password } = formValues;

  const dispacth = useDispatch();

  const state = useSelector((state) => state.ui);
  const { loading } = state;

  const handleSubmit = (ev) => {
    ev.preventDefault();
    dispacth(startLoginEmailPassword(email, password));
  };

  const handleGoogleLogin = () => {
    dispacth(startGoogleLogin());
  };

  return (
    <div className="auth__box">
      <h2 className="auth__title ">Login</h2>
      <form onSubmit={handleSubmit} className="auth__box">
        <input
          className="auth__box-input mb-5"
          name="email"
          value={email}
          onChange={handleInputChange}
          type="text"
          placeholder="email"
        />
        <input
          className="auth__box-input mb-5"
          name="password"
          value={password}
          onChange={handleInputChange}
          type="password"
          placeholder="password"
        />
        <button disabled={loading} className="btn-primary" type="submit">
          {" "}
          Login{" "}
        </button>
      </form>
 
      <div onClick={handleGoogleLogin} class="google-btn">
        <div class="google-icon-wrapper">
          <img
            class="google-icon"
            src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"
          alt="google-logo"
          />
        </div>
        <p class="btn-text">
          <b>Sign in with google</b>
        </p>
      </div>
      <Link to="../register">
        {" "}
        <p className="link"> Not register? Create account </p>
      </Link>
    </div>
  );
};

export default LoginScreen;
