import { getAuth } from "firebase/auth";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Routes, Route, Navigate } from "react-router-dom";
import JournalScreen from "../components/journal/JournalScreen";
import { firebaseApp } from "../firebase/firebase-config";
import { login } from "../redux/actions/authActions";
import {startLoadingNotes } from "../redux/actions/notesActions";
import AuthRouter from "./AuthRouter";

const AppRouter = () => {
  const [checked, setChecked] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const dispatch = useDispatch();
  useEffect(() => {
    const auth = getAuth(firebaseApp);

    auth.onAuthStateChanged( async (user) => {
      if (user?.uid) {
        dispatch(login(user.uid, user.displayName));
        setIsLoggedIn(true);
        dispatch(startLoadingNotes(user.uid))
      } else {
        setIsLoggedIn(false);
      }
      setChecked(true);
    });
  }, [dispatch, setChecked]);

  if (!checked) {
    //TODO: LOADINSCREEN;
    return <p> Loading...</p>;
  }

  return (
    <>
      {!isLoggedIn && (
        <Routes>
          <Route path="/*" element={<Navigate to="/auth/login" />} />
          <Route path="/auth/*" element={<AuthRouter />} />
        </Routes>
      )}
      {isLoggedIn && (
        <Routes>
          <Route path="/" element={<JournalScreen />} />
          <Route path="/*" element={<Navigate to="/" />} />
        </Routes>
      )}
    </>
  );
};

export default AppRouter;
